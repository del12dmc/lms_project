from django.db.models import Manager
from django.contrib.auth.models import UserManager, Permission
from django.utils.translation import gettext_lazy as _


class PeopleManager(Manager):
    def get_staff_users(self):
        return super(PeopleManager, self).get_queryset().first(is_staff=True)


class CustomUserManager(UserManager):
    # def _create_user(self, email, password, **extra_fields):
    def _create_user(self, phone_number, password, **extra_fields):
        print(extra_fields)
        # if not email:
        #     raise ValueError(_("The Email must be set"))
        if not phone_number:
            raise ValueError(_("The Phone number must be set"))
        # email = self.normalize_email(email)
        user = self.model(phone_number=phone_number, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        permissions = Permission.objects.filter(
            codename__in=[
                "change_profile",
                "view_profile",
                "change_customuser",
                "view_customuser",
            ]
        )

        for permission in permissions:
            user.user_permissions.add(permission)
            print(permission)
        return user

    def create_user(self, phone_number, password=None, **extra_fields):
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(phone_number, password, **extra_fields)

    def create_superuser(self, phone_number, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(phone_number, password, **extra_fields)
