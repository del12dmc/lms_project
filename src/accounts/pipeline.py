from social_core.backends.facebook import FacebookOAuth2


def clean_up_social_account(backend, uid, user=None, *args, **kwargs):
    print(kwargs)
    print(user)
    print("backend = ", backend)
    if isinstance(backend, FacebookOAuth2):
        print(" Facebook backend ")
        user.username = kwargs["username"]
        user.profile.first_name = kwargs["details"]["first_name"]
        user.profile.last_name = kwargs["details"]["last_name"]

    else:
        user.username = kwargs["response"]["login"]
        user.profile.photo = kwargs["response"]["avatar_url"]

    user.save()
    user.profile.save()
    return {"user": user}
