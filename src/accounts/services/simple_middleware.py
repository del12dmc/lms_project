import datetime


class SimpleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request, *args, **kwargs):
        print("before request")
        # request.current_time = datetime.datetime.now()
        setattr(request, "current_time", datetime.datetime.now())
        response = self.get_response(request)
        print("after request")
        print("request: ", response)

        return response
